.PHONY: test
Test_dir = pruebas
caso1 = "A1"
caso2 = "A8"
caso3 = "C4"
caso4 = "D4"
caso5 = "F8"
caso6 = "G2"
caso7 = "H1"
caso8 = "H8"
caso9 = "A6"
caso10 = "C8" 

Reinas: 
	@gcc Reinas.c -o Reinas 

test:
	@echo "Comprobando casos"
	@echo "*******************************************"
	@echo "Caso 1"
	@./Reinas $(caso1) > $(Test_dir)/output1.txt
	@diff -c $(Test_dir)/output1.txt $(Test_dir)/$(caso1).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 2"
	@./Reinas $(caso2) > $(Test_dir)/output2.txt
	@diff -c $(Test_dir)/output2.txt $(Test_dir)/$(caso2).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 3"
	@./Reinas $(caso3) > $(Test_dir)/output3.txt
	@diff -c $(Test_dir)/output3.txt $(Test_dir)/$(caso3).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 4"
	@./Reinas $(caso4) > $(Test_dir)/output4.txt
	@diff -c $(Test_dir)/output4.txt $(Test_dir)/$(caso4).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 5"
	@./Reinas $(caso5) > $(Test_dir)/output5.txt
	@diff -c $(Test_dir)/output5.txt $(Test_dir)/$(caso5).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 6"
	@./Reinas $(caso6) > $(Test_dir)/output6.txt
	@diff -c $(Test_dir)/output6.txt $(Test_dir)/$(caso6).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 7"
	@./Reinas $(caso7) > $(Test_dir)/output7.txt
	@diff -c $(Test_dir)/output7.txt $(Test_dir)/$(caso7).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 8"
	@./Reinas $(caso8) > $(Test_dir)/output8.txt
	@diff -c $(Test_dir)/output8.txt $(Test_dir)/$(caso8).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 9"
	@./Reinas $(caso9) > $(Test_dir)/output9.txt
	@diff -c $(Test_dir)/output9.txt $(Test_dir)/error$(caso9).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Caso 10"
	@./Reinas $(caso10) > $(Test_dir)/output10.txt
	@diff -c $(Test_dir)/output10.txt $(Test_dir)/error$(caso10).txt
	@echo "Correcto"
	@echo "*******************************************"
	@echo "Test realizado correctamente"
