#include <stdio.h>
#include <ctype.h>

#define FOR(var)  for (var = 0; var < 8; var++)

char tablero [8][8];

void inicializar_tablero() 
{
    int i, j;

    FOR (j) 
	{
        for (i = 0; i < 8; ++i)
            tablero[i][j] = '.';
    }
}

void imprimir_tablero() 
{

    printf("  ------------------\n");

    for(int i = 7; i>=0;i--) 
	{
        printf("%d |", i+1);
        for(int j = 0; j < 8; j++)
		{
            printf("%c ", tablero[i][j]);
		}
		printf("|\n");
    }
    printf("  ------------------");
	    printf("\n   ");
    for(int i = 0; i<8;i++) 
	{
        printf("%c ", 'A' + i);
	}
    printf("\n");
}
void colocar_reina(int i, int j, char tablero[8][8])
{

	tablero[i][j] = '#';

}

void movimiento(int i, int j)
{

	
	for(int k = i, l = j; k < 8 && l < 8; k++, l++)
	{
		tablero[k][l] = '1';
	}
    for(int k = i, l = j; k< 8 && l >= 0; k++, l-- )
	{ //diagonal descendent -->
		tablero[k][l] = '1';
    }
    for(int k = i, l = j; k>= 0 && l >= 0; k--, l-- )
	{ //diagonal descendent <--
		tablero[k][l] = '1';
    }
    for(int k = i, l = j; k >= 0 && l < 8; k--, l++ )
	{ //diagonal ascendent <--
		tablero[k][l] = '1';
    }
    for(int l = 0; l < 8;l++)
	{ //vertical
		tablero[i][l] = '1';
    }
    for(int k = 0; k < 8;k++)
	{ //horitzontal
		tablero[k][j] = '1';
    }
}

int main(int argc, char* argv[]) 
{
	int j = argv[1][0] - 'A';
	int i = argv[1][1] - '1';
    inicializar_tablero();
	movimiento(i, j);
	colocar_reina(i, j, tablero);
	imprimir_tablero();
     
}
